import { NgModule } from '@angular/core';
import { SearchComponent } from './Components/search/search.component';
import { AddAddressComponent } from './Components/add-address/add-address.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'Search', component: SearchComponent },
  { path: 'Add-Address', component: AddAddressComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
