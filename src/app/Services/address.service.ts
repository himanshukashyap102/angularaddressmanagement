import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { Address } from '../Model/Model';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  address: Address;
  constructor(private http: HttpClient) {
    this.address = new Address();
  }
  addData(addressDetails: any): Observable<any> {
    console.log(addressDetails);
    return this.http.post<any>('http://localhost:3000/address', addressDetails)
  }
  saveDataFromFile(field: any): Observable<any> {
    console.log(field);
    return this.http.post<any>('http://localhost:3000/address', field)
  }
  fetchFromDb(keyword:string){
    return this.http.get<Address[]>('http://localhost:3000/address',{params:new HttpParams().set('keyword',keyword)})
  }


}
