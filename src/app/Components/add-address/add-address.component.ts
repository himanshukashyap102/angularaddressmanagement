import { Component, OnInit } from '@angular/core';
import { Address } from 'src/app/Model/Model';
import { AddressService } from 'src/app/Services/address.service';
import * as XLSX from 'xlsx';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.css']
})
export class AddAddressComponent implements OnInit {
  address: Address;
  addressForm:FormGroup
  fileContent: string | any = '';
  file= new File([],'');
  formFlag:boolean=true;
  // addressDetail:Array<string>;
  constructor(private service: AddressService, private formBuilder:FormBuilder) {
    this.address = new Address();
    this.addressForm = this.formBuilder.group({
      address1:['',Validators.compose([Validators.required,
       Validators.minLength(3)])],
      city:['',Validators.compose([Validators.required,
       Validators.minLength(3)])],
      state:['',Validators.compose([Validators.required,
        Validators.minLength(3)])],
      zip:['',Validators.compose([Validators.required,
        Validators.pattern('[0-9]{6}')])],
     })
 
  }

  ngOnInit(): void {
  }
  filecloser(){
    this.formFlag=!this.formFlag ;
  }
  AddDetails(addressForm:FormGroup): void {

    let addressDetail = this.address.address1 + ", " + this.address.address2 + ", " + this.address.city + ", " + this.address.state + ", " + this.address.zip;
    let addr = { address: addressDetail };
    this.service.addData(addr).subscribe(resp=>{
      alert("Address saved Successfully!!!");
      
    },error=>{
      alert("oops");
    });
  }


  public readFile(event: any): void {

     this.file = event.target.files[0];
  
  }
  
  uploadFile(){
    let fileReader: FileReader = new FileReader();
    fileReader.readAsBinaryString(this.file);

    fileReader.onload = (x) => {
      var workBook = XLSX.read(fileReader.result, { type: 'binary' });
      var sheetNames = workBook.SheetNames;
      this.fileContent = XLSX.utils.sheet_to_json(workBook.Sheets[sheetNames[0]]);
      console.log(this.fileContent)
      for (var field of this.fileContent) {
        this.service.saveDataFromFile(field).subscribe(resp=>{
          alert("Address saved Successfully!!!");
          
        },error=>{
          alert("oops");
        });
      }

    };
  }

}
