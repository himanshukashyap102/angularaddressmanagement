import { Component, OnInit } from '@angular/core';
import { AddressService } from 'src/app/Services/address.service';
import { Address } from 'src/app/Model/Model';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchtext='';
  similarSearchAddresses:Array<Address>=[];
  searchFlag:boolean=true;
  length:number=0;
  constructor(private service: AddressService) {
  }

  ngOnInit(): void {
  }
  public search(){
    this.service.fetchFromDb(this.searchtext)
    .subscribe(resp=>{
      this.similarSearchAddresses=resp
      console.log(this.similarSearchAddresses)
      for(let addr of this.similarSearchAddresses){
        let stringvar=addr.address;
        let adrresspart=stringvar.split(',')
        console.log(adrresspart)
        addr.addressTitle=adrresspart[0]+", "+adrresspart[1];
        addr.addressDesc=adrresspart[adrresspart.length-3]+", "+adrresspart[adrresspart.length-2]+", "+adrresspart[adrresspart.length-1];
      }
    }
    )
    this.searchFlag=false;
  }

  }



